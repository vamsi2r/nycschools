//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import UIKit

class SchoolViewModel: NSObject {
    enum Section {
        case school
        case summary
        case location
        case sats
    }

    private let school: School
    private var sat: Sat?
    var sections:[Section] = [.school, .summary, .location]
    
    init(school: School, sat: Sat? = nil) {
        self.school = school
        self.sat = sat
        if sat != nil {
            sections.append(.sats)
        }
    }
    
    func title(for section: Section) -> String {
        switch section {
        case .school:
            return "School Name"
        case .summary:
            return "Summary"
        case .location:
            return "Address"
        case .sats:
            return "SAT"
        }
    }
    
    func content(for section: Section) -> String {
        switch section {
        case .school:
            return school.schoolName
        case .summary:
            return school.overview
        case .location:
            return "\(school.addressLine), \(school.city), \(school.stateCode), \(school.zip)"
        case .sats:
            var string = ""
            if let sat = sat {
                string = "Number of Sat test takers: \(sat.numOfSatTestTakers)\nReading average score: \(sat.readingAvgScore)\nMath average score: \(sat.mathAvgScore)\nWriting average score: \(sat.writingAvgScore)"
            }
            return string
        }
    }
}
