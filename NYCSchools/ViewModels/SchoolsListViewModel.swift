//
//  SchoolsListViewModel.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import Foundation
import Combine

class SchoolsListViewModel: NSObject {
    
    enum UIState {
        case none
        case fetching
        case fetched
        case error
    }
    @Published var state: UIState = .none
    var schools = [School]()
    private var sats = [Sat]()
    private var cancellables = Set<AnyCancellable>()
    
    override init() {
        super.init()
        self.fetchSats()
    }
    
    private func fetchSchools() {
        Network.getSchools()
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    print("HighSchools Fetch failed: \(error.localizedDescription)")
                    self?.state = .error
                case .finished:
                    print("HighSchools Fetch completed")
                    self?.state = .fetched
                }
            } receiveValue: { [weak self] items in
                self?.schools.append(contentsOf: items)
                self?.schools.sort(by: { $0.schoolName < $1.schoolName })
            }
            .store(in: &cancellables)
    }

    private func fetchSats() {
        self.state = .fetching
        Network.getSats()
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    print("Sats Fetch failed: \(error.localizedDescription)")
                    self?.state = .error
                case .finished:
                    print("Sats Fetch completed")
                    self?.fetchSchools()
                }
            } receiveValue: { [weak self] items in
                self?.sats.append(contentsOf: items)
            }
            .store(in: &cancellables)
    }
    
    func findSat(for id: String) -> Sat? {
        return self.sats.first { item in
            item.id == id
        }
    }
}
