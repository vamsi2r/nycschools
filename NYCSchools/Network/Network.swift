//
//  Network.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import Foundation
import Combine

struct Network {
    
    enum NetworkURL {
        case schools
        case sats
        
        private static let host = "https://data.cityofnewyork.us/resource/"
        
        func url() -> URL {
            let url = URL(string: NetworkURL.host)!
            switch self {
            case .schools:
                return url.appending(path: "s3k6-pzi2.json")
            case .sats:
                return url.appending(path: "f9bf-2cp4.json")
            }
        }
    }
    
    private static func getData<T: Decodable>(_ type: T.Type, url: URL) -> AnyPublisher<T, Error> {
        let urlSession = URLSession.shared
        let decoder = JSONDecoder()
        return urlSession.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: T.self, decoder: decoder)
            .eraseToAnyPublisher()
    }
    
    static func getSchools() -> AnyPublisher<[School], Error> {
        return Network.getData([School].self, url: NetworkURL.schools.url())
    }

    static func getSats() -> AnyPublisher<[Sat], Error> {
        return Network.getData([Sat].self, url: NetworkURL.sats.url())
    }

}
