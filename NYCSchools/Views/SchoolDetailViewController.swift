//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import UIKit

class SchoolDetailViewController: UITableViewController {

    var viewModel: SchoolViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "School Details"
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.title(for: viewModel.sections[section])
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailCell

        // Configure the cell...
        cell.configureCell(viewModel.content(for: viewModel.sections[indexPath.section]))
        return cell
    }
}
