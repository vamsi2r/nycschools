//
//  HighSchoolCell.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import UIKit

class HighSchoolCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(_ school: School) {
        self.nameLabel.text = school.schoolName
    }
}
