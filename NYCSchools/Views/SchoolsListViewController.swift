//
//  SchoolsListViewController.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import UIKit
import Combine

class SchoolsListViewController: UITableViewController {

    var viewModel = SchoolsListViewModel()
    private var cancellables = Set<AnyCancellable>()
    @IBOutlet weak var loadingView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBinding()
    }
    
    func setupBinding() {
        self.viewModel.$state
            .receive(on: DispatchQueue.main)
            .sink { state in
                switch state {
                case .fetching:
                    self.tableView.tableHeaderView = self.loadingView
                case .fetched:
                    self.tableView.reloadData()
                    self.tableView.tableHeaderView = nil
                case .error:
                    //Display error:
                    self.tableView.tableHeaderView = nil
                    break
                case .none:
                    //Do nothing
                    break
                }
            }
            .store(in: &cancellables)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HighSchoolCell", for: indexPath) as! HighSchoolCell

        // Configure the cell...
        cell.configureCell(viewModel.schools[indexPath.row])

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = viewModel.schools[indexPath.row]
        let sat = viewModel.findSat(for: school.id)
        let schoolViewModel = SchoolViewModel(school: school, sat: sat)
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SchoolDetailViewController") as? SchoolDetailViewController {
            viewController.viewModel = schoolViewModel
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
