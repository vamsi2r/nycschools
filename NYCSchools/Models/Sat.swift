//
//  Sat.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import UIKit

struct Sat: Codable {
    var id: String
    let schoolName: String
    let numOfSatTestTakers: String
    let readingAvgScore: String
    let mathAvgScore: String
    let writingAvgScore: String

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}

