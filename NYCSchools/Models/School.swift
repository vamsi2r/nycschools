//
//  School.swift
//  NYCSchools
//
//  Created by Vamsi Rajulapudi on 8/21/23.
//

import Foundation

struct School: Codable {
    var id: String
    let schoolName: String
    let overview: String
    let addressLine: String
    let city: String
    let zip: String
    let stateCode: String

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case overview = "overview_paragraph"
        case addressLine = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
    }
}
